// @ts-check
const
  express = require('express'),
  ews = require('express-ws'), // enable websocket support
  CMWServer = require('..'); // replace with @cern/cmw-express

/**
 * @typedef {import('..').Config} Config
 */

/** @type {cmwexpress.Config} */
var config;
try {
  // @ts-ignore
  config = require('/etc/app/config')
}
catch (e) {
  config = require('./config');
}


const app = express();
ews(app);

const router = express.Router();

const service = new CMWServer(config);
service.register(router);

app.use(config.basePath, router);

/* test part */

app.listen(8080);
