
const
  debug = require('debug')('test'),
  { get } = require('lodash'),
  { describe, it, afterEach } = require('mocha'),
  express = require('express'),
  ews = require('express-ws'),
  { expect } = require('chai'),
  axios = require('axios'),
  { makeDeferred } = require('@cern/prom'),
  RDA3Device = require('./RDA3Device'),
  WebSocket = require('ws'),
  CMWServer = require('..');

describe('CMWServer WS', function() {
  /** @type {{
      service: CMWServer,
      server: http.Server,
      device: RDA3Device,
      ws: WebSocket
    }}
  */
  var env = {};

  afterEach(function() {
    env.device?.release();
    env.device = null;

    env.service?.release();
    env.service = null;

    env.server?.close();
    env.server = null;

    env.ws?.close();
    env.ws = null;
  });

  it('receives an error when monitor is disabled', async function() {
    this.timeout(15000);
    env.device = new RDA3Device('cmw-express-test-ev2.localhost');
    const app = express();

    env.service = new CMWServer({ endpoints: [
      { server: 'cmw-express-test-ev2.localhost',
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        monitor: false }
    ] });
    ews(app);
    env.service.register(app);
    env.server = app.listen(8080);

    /* ensure we can GET */
    await axios.get('http://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');

    const def = makeDeferred();
    env.ws = new WebSocket('ws://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');
    env.ws.on('message', (msg) => debug('message received: %o', msg));
    env.ws.onopen = () => debug('websocket opened');
    env.ws.onerror = (err) => debug('websocket error', err);
    env.ws.onclose = (err) => def.resolve(err);

    const ret = await def.promise;
    expect(ret).to.deep.contains({ code: 1005 });
  });
});
