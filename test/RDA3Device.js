// @ts-check

const
  { forEach, unset, size, get } = require('lodash'),
  RDA3 = require('@cern/rda3'),
  debug = require('debug')('test:stub');

/**
 * @param  {any} initial
 * @return {RDA3.AcquiredData}
 */
function makeProperty(initial) {
  const data = new RDA3.AcquiredData();
  data.cycleName = "MyCycle";
  data.cycleStamp = BigInt(0);
  data.acqStamp = BigInt(0);
  data.data = new RDA3.Data();
  forEach(initial, (value, key) => data.data.append(key, value));
  return data;
}

class RDA3Device {
  /**
   * @param {string} name
   * @details
   * - TEST_PROPERTY can be subscribed and get
   * - TEST_SETTING can be set
   */
  constructor(name, interval = 1000) {
    this.srv = new RDA3.Server(name);
    this.count = 0;

    this.props = {
      TEST_PROPERTY: {
        read: true, listeners: {}, data: makeProperty({ msg: this.count }) },
      TEST_SETTING: {
        read: true, write: true, data: makeProperty({ setval: 42 }) }
    };

    this.srv.setSubSrcAddedCallback((req) => this.subSrcAdded(req));
    this.srv.setSubSrcRemovedCallback((req) => this.subSrcRemoved(req));
    this.srv.setSubscribeCallback((req) => this.sub(req));
    this.srv.setGetCallback((req) => this.get(req));
    this.srv.setSetCallback((req) => this.set(req));
    this.loop = setInterval(() => this.onTick(), interval);
    debug('starting device');
    this.srv.start();
  }

  release() {
    debug('stopping device');
    this.srv.stop();
    clearInterval(this.loop);
  }

  /** @param {RDA3.Server.SubscriptionSource} req */
  subSrcAdded(req) {
    const prop = this.props[req.Property];
    if (!prop) {
      throw new Error("unknown property");
    }
    else if (!prop.listeners) {
      throw new Error("property not subscribable");
    }
    debug('adding sub %i on prop %s', req.Id, req.Property);
    prop.listeners[req.Id] = req;
  }

  /** @param {RDA3.Server.SubscriptionSource} req */
  subSrcRemoved(req) {
    const prop = this.props[req.Property];
    if (!prop) { throw new Error("unknown property"); }
    debug('removing sub %i', req.Id);
    unset(prop, req.Id);
  }

  /** @param {RDA3.Server.Request} req */
  sub(req) {
    const prop = this.props[req.Property];
    if (!prop) { throw new Error("unknown property"); }
    debug('client get value on prop %s', req.Property);
    return prop.data;
  }

  /** @param {RDA3.Server.Request} req */
  get(req) {
    const prop = this.props[req.Property];
    if (!prop) { throw new Error("unknown property"); }
    debug('client get value on prop %s', req.Property);
    return prop.data.data;
  }

  /** @param {RDA3.Server.Request} req */
  set(req) {
    const prop = this.props[req.Property];
    if (!prop) {
      throw new Error("unknown property");
    }
    if (!prop.write) {
      throw new Error("property not writable");
    }
    debug('client set value on prop %s', req.Property);
    forEach(get(req, [ 'Data', 'entries' ]), (value, key) => {
      prop.data.data.remove(key);
      prop.data.data.append(key, value);
    });
  }

  onTick() {
    const prop = this.props.TEST_PROPERTY;
    prop.data.data.remove('msg');
    prop.data.data.append('msg', ++this.count);
    debug('sending %i notifications', size(prop.listeners));
    // @ts-ignore
    forEach(prop.listeners, (l) => l.notify(prop.data));
  }
}

// @ts-ignore
if (require.main === module) {
  // @ts-ignore
  const device = new RDA3Device(process.argv[2] || 'test-device.localhost'); /* jshint unused:false */
}
else {
  module.exports = RDA3Device;
}
