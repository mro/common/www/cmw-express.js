
const
  debug = require('debug')('test'),
  { get } = require('lodash'),
  { describe, it, afterEach } = require('mocha'),
  express = require('express'),
  ews = require('express-ws'),
  { expect } = require('chai'),
  { makeDeferred } = require('@cern/prom'),
  RDA3Device = require('./RDA3Device'),
  WebSocket = require('ws'),
  CMWServer = require('..');

describe('CMWServer', function() {
  /** @type {{
      service: CMWServer,
      server: http.Server,
      device: RDA3Device
    }}
  */
  var env = {};

  afterEach(function() {
    if (env.device) {
      env.device.release();
      env.device = null;
    }
    if (env.service) {
      env.service.release();
      env.service = null;
    }
    if (env.server) {
      env.server.close();
      env.server = null;
    }
  });

  it('can retrieve throttled values', async function() {
    this.timeout(4000);
    env.device = new RDA3Device('cmw-express-test-throttle.localhost', 100);
    const app = express();
    env.service = new CMWServer({ endpoints: [
      { server: 'cmw-express-test-throttle.localhost',
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        throttle: 1000 }
    ] });
    ews(app);
    env.service.register(app);
    env.server = app.listen(8080);

    const def = makeDeferred();
    const ws = new WebSocket('ws://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');
    var msgs = [];
    ws.on('message', (msg) => {
      debug('message received: %o', msg);
      msgs.push(get(JSON.parse(msg), [ 'data', 'entries', 'msg' ]));
      if (msgs.length >= 2) {
        def.resolve();
        ws.close();
      }
    });
    ws.onopen = () => debug('websocket opened');
    ws.onerror = (err) => debug('websocket error', err);
    await def.promise;

    // more than 5 ticks should have elapsed (about 10)
    expect(msgs[1]).to.be.gt(msgs[0] + 5);
  });
});
