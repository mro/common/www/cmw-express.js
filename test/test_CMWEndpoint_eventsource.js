
const
  debug = require('debug')('test'),
  { get } = require('lodash'),
  { describe, it, afterEach } = require('mocha'),
  express = require('express'),
  ews = require('express-ws'),
  { expect } = require('chai'),
  axios = require('axios'),
  { makeDeferred } = require('@cern/prom'),
  RDA3Device = require('./RDA3Device'),
  EventSource = require('eventsource'),
  CMWServer = require('..');

describe('CMWServer SSE', function() {
  /** @type {{
      service: CMWServer,
      server: http.Server,
      device: RDA3Device,
      ev: EventSource
    }}
  */
  var env = {};

  afterEach(function() {
    if (env.device) {
      env.device.release();
      env.device = null;
    }
    if (env.service) {
      env.service.release();
      env.service = null;
    }
    if (env.server) {
      env.server.close();
      env.server = null;
    }
    if (env.ev) {
      env.ev.close();
      env.ev = null;
    }
  });

  it('supports text/event-source streams', async function() {
    this.timeout(4000);
    env.device = new RDA3Device('cmw-express-test-ev.localhost', 100);
    const app = express();

    env.service = new CMWServer({ endpoints: [
      { server: 'cmw-express-test-ev.localhost',
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        throttle: 1000 }
    ] });
    ews(app);
    env.service.register(app);
    env.server = app.listen(8080);

    const def = makeDeferred();
    const msgs = [];
    env.ev = new EventSource('http://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');
    env.ev.onopen = () => debug('event-source opened');
    env.ev.onerror = (err) => debug('event-source error', err);
    env.ev.on('error', (msg) => debug('event-source error message', msg));
    env.ev.on('message', (msg) => {
      debug('message received: %o', msg);
      msgs.push(get(JSON.parse(msg.data), [ 'data', 'entries', 'msg' ]));
      if (msgs.length >= 2) {
        env.ev.close();
        def.resolve();
      }
    });
    await def.promise;

    // more than 5 ticks should have elapsed (about 10)
    expect(msgs[1]).to.be.gt(msgs[0] + 5);
  });

  it('receives errors from text/event-source', async function() {
    this.timeout(4000);
    env.device = new RDA3Device('cmw-express-test-ev2.localhost', 100);
    const app = express();

    env.service = new CMWServer({ endpoints: [
      { server: 'cmw-express-test-ev2.localhost',
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        throttle: 1000 }
    ] });
    ews(app);
    env.service.register(app);
    env.server = app.listen(8080);

    // will generate an error on server side
    env.device.srv.setSubscribeCallback(() => null);
    env.device.srv.setGetCallback(() => null);

    const def = makeDeferred();
    env.ev = new EventSource('http://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');
    env.ev.onopen = () => debug('event-source opened');
    env.ev.onerror = (err) => debug('event-source error', err);
    env.ev.on('error', (msg) => {
      debug('event-source error message', msg);
      env.ev.close();
      def.resolve(msg);
    });
    env.ev.on('message', (msg) => debug('message received: %o', msg));
    const msg = await def.promise;

    expect(msg).to.deep.contains({ type: 'error' });
  });

  it('receives an error when monitor is disabled', async function() {
    this.timeout(15000);
    env.device = new RDA3Device('cmw-express-test-ev2.localhost', 100);
    const app = express();

    env.service = new CMWServer({ endpoints: [
      { server: 'cmw-express-test-ev2.localhost',
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        monitor: false }
    ] });
    ews(app);
    env.service.register(app);
    env.server = app.listen(8080);

    /* ensure we can GET */
    await axios.get('http://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');

    const def = makeDeferred();
    env.ev = new EventSource('http://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');
    env.ev.onopen = () => def.reject(new Error('should fail'));
    env.ev.onerror = (err) => def.resolve(err);
    env.ev.on('error', (msg) => debug('event-source error message', msg));
    env.ev.on('message', (msg) => debug('message received: %o', msg));
    const msg = await def.promise;

    expect(msg).to.deep.contains({ status: 404, type: 'error' });
  });
});
