// @ts-check
const
  { describe, it, afterEach } = require('mocha'),
  { expect } = require('chai'),
  { makeDeferred, delay } = require('@cern/prom'),
  Watchdog = require('../src/Watchdog');

describe('Watchdog', function() {
  /** @type {Watchdog|null} */
  var wd = null;

  afterEach(function() {
    if (wd) {
      wd.reset();
      wd = null;
    }
  });

  it('emits a signal when started', async function() {
    const prom = makeDeferred();
    wd = new Watchdog(1, 10);
    wd.on('expired', () => prom.resolve());
    wd.start();
    return prom;
  });

  it('does not emit once reset', async function() {
    this.timeout(5000);
    var called = false;

    wd = new Watchdog(1, 10);
    wd.on('expired', () => { called = true; });
    wd.start();
    expect(wd.errors).to.equal(1);
    wd.reset();

    await delay(2000);
    expect(wd.errors).to.equal(0);
    expect(called).to.equal(false);
  });

  it('does uses exponential back-off', async function() {
    this.timeout(5000);
    var called = 0;

    wd = new Watchdog(1, 10);
    wd.on('expired', () => {
      called++;
      wd.start(); /* trigger another error */
    });
    wd.start();

    await delay(2500);
    expect(called).to.equal(1);

    await delay(1000);
    expect(called).to.equal(2);

    wd.reset();
  });

  it('uses maxDelay', async function() {
    this.timeout(5000);
    var called = 0;

    wd = new Watchdog(1, 2);
    wd.on('expired', () => {
      called++;
      wd.start(); /* trigger another error */
    });
    wd.start();

    await delay(3500);
    expect(called).to.equal(2);

    wd.reset();
  });
});
