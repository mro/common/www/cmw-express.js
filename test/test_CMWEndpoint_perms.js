const
  debug = require('debug')('test'),
  { get, invoke } = require('lodash'),
  { describe, it, afterEach } = require('mocha'),
  // { fork } = require('child_process'),
  express = require('express'),
  ews = require('express-ws'),
  { expect } = require('chai'),
  axios = require('axios'),
  { makeDeferred } = require('@cern/prom'),
  RDA3Device = require('./RDA3Device'),
  WebSocket = require('ws'),
  CMWServer = require('..');

describe('CMWServer perms', function() {
  /** @type {{
      service: CMWServer,
      server: http.Server,
      device: RDA3Device,
      interval?: NodeJS.Interval
    }}
  */
  var env = {};
  var id = 0;

  afterEach(function() {
    invoke(env, [ 'device', 'release' ]);
    env.device = null;
    invoke(env, [ 'service', 'release' ]);
    env.service = null;
    invoke(env, [ 'server', 'close' ]);
    env.server = null;
    clearInterval(env.interval);
    env.interval = null;
  });

  it('can edit properties', async function() {
    env.device = new RDA3Device(`cmw-express-test-perms${++id}.localhost`);

    const app = express();
    env.service = new CMWServer({ endpoints: [
      { server: `cmw-express-test-perms${id}.localhost`,
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_SETTING',
        write: true }
    ] });
    env.service.register(app);

    env.server = app.listen(8080);

    const url = 'http://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_SETTING';
    let ret = await axios.get(url);
    expect(ret).to.have.nested.property('data.data.entries.setval', 42);

    await axios.put(url, { setval: 44, useless: 12 });

    ret = await axios.get(url);
    expect(ret).to.have.nested.property('data.data.entries.setval', 44);
  });

  it('fails to edit readonly property', async function() {
    env.device = new RDA3Device(`cmw-express-test-perms${++id}.localhost`);

    const app = express();
    env.service = new CMWServer({ endpoints: [
      { server: `cmw-express-test-perms${id}.localhost`,
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        write: false }
    ] });
    env.service.register(app);

    env.server = app.listen(8080);

    const url = 'http://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY';
    const ret = await axios.get(url);
    expect(ret).to.have.nested.property('data.data.entries.msg');

    await axios.put(url, { msg: 44 })
    .then(
      () => expect.fail('should fail'),
      (err) => expect(err).to.have.nested.property('response.status', 404));
  });

});
