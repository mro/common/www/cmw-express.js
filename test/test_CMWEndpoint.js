
const
  debug = require('debug')('test'),
  { get, invoke } = require('lodash'),
  { describe, it, afterEach } = require('mocha'),
  // { fork } = require('child_process'),
  express = require('express'),
  ews = require('express-ws'),
  { expect } = require('chai'),
  axios = require('axios'),
  { makeDeferred } = require('@cern/prom'),
  RDA3Device = require('./RDA3Device'),
  WebSocket = require('ws'),
  CMWServer = require('..');

describe('CMWServer', function() {
  /** @type {{
      service: CMWServer,
      server: http.Server,
      device: RDA3Device,
      interval?: NodeJS.Interval
    }}
  */
  var env = {};
  var id = 0;

  afterEach(function() {
    invoke(env, [ 'device', 'release' ]);
    env.device = null;
    invoke(env, [ 'service', 'release' ]);
    env.service = null;
    invoke(env, [ 'server', 'close' ]);
    env.server = null;
    clearInterval(env.interval);
    env.interval = null;
  });

  it('can retrieve a value', async function() {
    env.device = new RDA3Device(`cmw-express-test${++id}.localhost`);

    const app = express();
    env.service = new CMWServer({ endpoints: [
      { server: `cmw-express-test${id}.localhost`,
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        unsubscribeDelay: 1 }
    ] });
    env.service.register(app);

    env.server = app.listen(8080);

    const ret = await axios.get('http://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');
    debug('message received: %o', ret.data);
    expect(get(ret, [ 'data', 'data', 'entries', 'msg' ])).to.be.gte(0);
  });

  it('can use a websocket', async function() {
    env.device = new RDA3Device(`cmw-express-test${++id}.localhost`);
    const app = express();
    env.service = new CMWServer({ endpoints: [
      { server: `cmw-express-test${id}.localhost`,
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        unsubscribeDelay: 1 }
    ] });
    ews(app);
    env.service.register(app);
    env.server = app.listen(8080);

    const def = makeDeferred();
    const ws = new WebSocket('ws://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');
    ws.on('message', (msg) => {
      debug('message received: %o', msg);
      def.resolve(JSON.parse(msg));
      ws.close();
    });
    ws.onopen = () => debug('websocket opened');
    ws.onerror = (err) => debug('websocket error', err);
    const ret = await def.promise;
    expect(get(ret, [ 'data', 'entries', 'msg' ])).to.be.gte(0);
  });

  it('can receive errors from devices', async function() {
    env.device = new RDA3Device(`cmw-express-test${++id}.localhost`);
    const app = express();
    env.service = new CMWServer({ endpoints: [
      { server: `cmw-express-test${id}.localhost`,
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        unsubscribeDelay: 1 }
    ] });

    ews(app);
    env.service.register(app);

    // will generate an error on server side
    env.device.srv.setSubscribeCallback(() => null);
    env.device.srv.setGetCallback(() => null);
    // device.release();

    env.server = app.listen(8080);

    const ret = await axios.get('http://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');
    debug('message received: %o', ret.data);
    expect(ret.data).to.have.property('error');
  });

  it('can detect device disconnects', async function() {
    this.timeout(20000);
    env.device = new RDA3Device(`cmw-express-test${++id}.localhost`);
    const app = express();
    env.service = new CMWServer({ endpoints: [
      { server: `cmw-express-test${id}.localhost`,
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        unsubscribeDelay: 1 }
    ] });

    ews(app);
    env.service.register(app);
    env.server = app.listen(8080);

    const def = makeDeferred();
    const ws = new WebSocket('ws://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');
    ws.on('message', (msg) => {
      debug('message received: %o', msg);
      msg = JSON.parse(msg);
      if (env.device) { // first message, we disconnect
        env.device.release();
        env.device = null;
      }
      if (msg.error) { // second message an error
        def.resolve(msg);
        ws.close();
      }
    });
    ws.onopen = () => debug('websocket opened');
    ws.onerror = (err) => debug('websocket error', err);
    const ret = await def.promise;
    expect(ret).to.have.property('error');
  });

  it('can detect device re-connection', async function() {
    this.timeout(120000);
    // there's an issue re-starting same server in same app, let's spawn it aside
    // var child = fork(`${__dirname}/RDA3Device`, [ `cmw-express-test${++id}.localhost` ]);
    // env.device = { release: () => child.kill() };
    env.device = new RDA3Device(`cmw-express-test${++id}.localhost`);
    const app = express();
    env.service = new CMWServer({ endpoints: [
      { server: `cmw-express-test${id}.localhost`,
        device: 'CMW_EXPRESS_TEST_DEVICE', property: 'TEST_PROPERTY',
        unsubscribeDelay: 1 }
    ] });

    ews(app);
    env.service.register(app);
    env.server = app.listen(8080);
    var hadError = false;

    const def = makeDeferred();
    const ws = new WebSocket('ws://localhost:8080/CMW_EXPRESS_TEST_DEVICE/TEST_PROPERTY');
    ws.on('message', (msg) => {
      debug('message received: %o', msg);
      msg = JSON.parse(msg);
      if (env.device && !hadError) { // first message, we disconnect
        if (msg.error) { return; }
        env.device.release();
        env.device = null;
        env.interval = setInterval(() => {
          debug('restarting');
          if (env.device) {
            env.device.release();
            env.device = new RDA3Device(`cmw-express-test${id}.localhost`);
          }
        }, 15000);
      }
      if (msg.error) { // second message an error
        hadError = true;
        if (!env.device) {
          env.device = new RDA3Device(`cmw-express-test${id}.localhost`);
          // child = fork(`${__dirname}/RDA3Device`, [ `cmw-express-test${id}.localhost` ]);
          // env.device = { release: () => child.kill() };
        }
      }
      else if (hadError) { // third message reco
        def.resolve(msg);
        ws.close();
      }
    });
    ws.onopen = () => debug('websocket opened');
    ws.onerror = (err) => debug('websocket error', err);
    const ret = await def.promise;
    expect(get(ret, [ 'data', 'entries', 'msg' ])).to.be.gte(0);

    clearInterval(env.interval);
    env.interval = null;
  });
});
