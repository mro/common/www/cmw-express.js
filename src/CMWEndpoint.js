// @ts-check

const
  { get, has, throttle, isEmpty, forEach, toString } = require('lodash'),
  RDA3 = require('@cern/rda3'),
  debug = require('debug')('cmw:endpoint'),
  sse = require('./express-sse'),
  express = require('express'),
  Watchdog = require('./Watchdog');

// @ts-ignore
BigInt.prototype.toJSON = function() { return this.toString();  };
const WS_GOING_AWAY = 1001;
const WS_INTERNAL_ERROR = 1011;

/**
 * @typedef {express.IRouter} IRouter
 * @typedef {express.IRouter & import('express-ws').WithWebsocketMethod} WSIRouter
 */

class CMWEndpoint {
  /**
   * @param {cmwexpress.EPConfig} config
   */
  constructor(config) {
    this._id = 0;
    this.deviceName = get(config, [ 'device' ]);
    this.propName = get(config, [ 'property' ]);
    /** @type {cmwexpress.EPConfig} */
    this.config = config;
    this.canRead = get(config, [ 'read' ], true);
    this.canWrite = get(config, [ 'write' ], false);
    this.canMonitor = get(config, [ 'monitor' ], true);

    if (this.canMonitor) {
      this._dispatch = this.config.throttle ?
        throttle(this._dispatch.bind(this), this.config.throttle) :
        this._dispatch.bind(this);
      this._dispatchError = this.config.throttle ?
        throttle(this._dispatchError.bind(this), this.config.throttle) :
        this._dispatchError.bind(this);

      /** @type {NodeJS.Timeout|null} */
      this.unsubscribeTimeout = null;
      /** @type {{ [id: number]: ((err?: any, data?: any) => any) }} */
      this.connected = {};
      /** @type {{ [id: number]: ((err?: any, data?: any) => any) }} */
      this.sseConnected = {};
      /** @type {any} */
      this.value = null;
      this.subscribed = false;
      this.wd = new Watchdog();
      this.wd.on('expired', this._onWatchdogExpired.bind(this));
    }

    this.ap = new RDA3.AccessPoint(this.deviceName, this.propName,
      get(config, [ 'server' ], ''));
    this.ap.on('data', this._dispatch);
    this.ap.on('error', this._dispatchError);
  }

  /**
   * @param {any} data
   */
  _dispatch(data) {
    this.wd?.reset();
    this.value = data;
    forEach(this.connected, function(cb) { cb(undefined, data); });
    if (!isEmpty(this.sseConnected)) {
      const sseData = this.ssePrepare(data);
      forEach(this.sseConnected, function(cb) { cb(undefined, sseData); });
    }
  }

  /**
   * @param {any} err
   */
  _dispatchError(err) {
    this.wd?.start();
    this.value = { error: err };
    forEach(this.connected, (cb) => cb(this.value));
    if (!isEmpty(this.sseConnected)) {
      const sseData = toString(err);
      forEach(this.sseConnected, function(cb) { cb(sseData); });
    }
  }

  _onWatchdogExpired() {
    debug('error watchdog triggered, reconnecting');
    if (this.subscribed) {
      this.ap.unsubscribe();
    }
    this.ap.removeAllListeners();
    this.ap = new RDA3.AccessPoint(this.deviceName, this.propName,
      get(this.config, [ 'server' ], ''));
    this.ap.on('data', this._dispatch);
    this.ap.on('error', this._dispatchError);
    if (this.subscribed) {
      if (has(this.config, 'selector')) {
        this.ap.subscribe(this.config.selector);
      }
      else {
        this.ap.subscribe();
      }
      /* restart watchdog, will be cleared by first correct message */
      this.wd?.start();
    }
  }

  /**
   * @param  {any} value
   */
  ssePrepare(value) {
    return JSON.stringify(value).split('\n');
  }

  release() {
    debug('releasing %s->%s', this.deviceName, this.propName);
    if (this.subscribed) {
      this.ap.unsubscribe();
      this.subscribed = false;
    }
    if (this.unsubscribeTimeout !== null) {
      clearTimeout(this.unsubscribeTimeout);
      this.unsubscribeTimeout = null;
    }
    this.ap.removeAllListeners();
    forEach(this.connected, (cb) => cb());
    forEach(this.sseConnected, (cb) => cb());
    this.wd?.reset();
  }

  onTimeout() {
    this.unsubscribeTimeout = null;
    if (!isEmpty(this.connected) || !isEmpty(this.sseConnected)) { // unlikely
      debug('idle timeout whilst clients still connected');
      return;
    }

    debug('unsubscribing %s->%s', this.deviceName, this.propName);
    this.subscribed = false;
    this.ap.unsubscribe();
    this.value = null;
  }

  /**
   * @param {(err: any, data: any) => any} cb
   * @param {express.Request} req
   * @param {boolean} sse
   * @return {number} subscription id
   */
  onConnect(cb, req, sse = false) {
    const id = this._id++;
    if (sse) {
      this.sseConnected[id] = cb;
    }
    else {
      this.connected[id] = cb;
    }

    if (this.unsubscribeTimeout !== null) {
      clearTimeout(this.unsubscribeTimeout);
      this.unsubscribeTimeout = null;
    }
    if (!this.subscribed) {
      debug('subscribing %s->%s', this.deviceName, this.propName);
      try {
        this.subscribed = true;
        this.ap.subscribe(this.config?.selector, this.getAuthInfo(req));
      }
      catch (err) {
        this.subscribed = false;
        throw err;
      }
    }
    return id;
  }

  /**
   * @param {number} id
   */
  onDisconnect(id) {
    delete this.sseConnected[id];
    delete this.connected[id];
    if (isEmpty(this.connected) && isEmpty(this.sseConnected) &&
        this.config.unsubscribeDelay && this.subscribed) {
      debug('starting unsubscribeDelay %s->%s', this.deviceName, this.propName);
      this.unsubscribeTimeout = setTimeout(this.onTimeout.bind(this),
        this.config.unsubscribeDelay * 1000);
    }
  }

  /**
   * @param {express.Request} req
   * @return {string|undefined}
   */
  getSelector(req) {
    const selector = get(req.query, [ 'selector' ]) ||
      get(this.config, [ 'selector' ]);
    return selector ? toString(selector) : undefined;
  }

  /**
   * @param {express.Request} req
   * @return {RDA3.Data}
   */
  getAuthInfo(req) {
    const context = this.ap.createData();
    for (const key of [ "sub", "email" ]) {
      // @ts-ignore
      const value = req?.user?.[key];
      if (value) {
        context.append(key, value);
      }
    }
    return context;
  }

  /**
   * @param {express.Request} req
   * @param {express.Response} res
   */
  put(req, res) {
    debug("put %s->%s", this.deviceName, this.propName);
    const data = this.ap.createData();
    forEach(req.body, (value, key) => {
      if (has(value, 'value')) {
        data.append(key, value.value, value.type);
      }
      else {
        data.append(key, value);
      }
    });

    return this.ap.set(data, this.getSelector(req), this.getAuthInfo(req))
    .then(
      (/** @type {any} */ ret) => res.json(ret),
      (/** @type {any} */ err) => res.json({ error: err }));
  }

  /**
   * @param {express.Request} req
   * @param {express.Response} res
   */
  get(req, res) {
    debug("get %s->%s", this.deviceName, this.propName);
    return this.ap.get(this.getSelector(req), this.getAuthInfo(req))
    .then(
      (/** @type {any} */ ret) => res.json(ret),
      (/** @type {any} */ err) => res.json({ error: err }));
  }

  /**
   * @param  {IRouter} router
   * @param  {string} path
   */
  register(router, path) {
    router.get(path, sse, (req, res, next) => {
      if (!res.sse) {
        if (!this.canRead) { return next(); }

        return this.get(req, res);
      }
      else if (!this.canMonitor) {
        return next();
      }
      else {
        try {
          const id = this.onConnect((err, data) => {
            if (!res.sse) { return; }
            if (err !== undefined) {
              res.sse(err, { type: 'error' });
            }
            else if (data !== undefined) {
              res.sse(data);
            }
            else {
              res.sse('server shutdown', { type: 'error' });
              res.end();
            }
          }, req, true);
          const shutdown = () => {
            res.removeAllListeners();
            this.onDisconnect(id);
          };
          if (this.value !== null) {
            res.sse(this.ssePrepare(this.value));
          }

          res.once('close', shutdown);
          res.once('finish', shutdown);
        }
        catch (/** @type {any} */ err) {
          res.sse(err?.message ?? 'internal error', { type: 'error' }).end();
        }
      }
    });

    // @ts-ignore
    if (this.canMonitor && router.ws) {
      /** @type {WSIRouter} */ (router).ws(path, (ws, req) => {
        try {
          const id = this.onConnect((err, data) => {
            if (err) {
              ws.send(JSON.stringify(err));
            }
            else if (data !== undefined) {
              ws.send(JSON.stringify(data));
            }
            else {
              ws.close(WS_GOING_AWAY);
            }
          }, req);
          if (this.value !== null) {
            ws.send(JSON.stringify(this.value));
          }
          ws.once('close', () => {
            this.onDisconnect(id);
          });
        }
        catch (/** @type {any} */ err) {
          ws.close(WS_INTERNAL_ERROR, err?.message);
        }
      });
    }

    if (this.canWrite) {
      router.put(path, CMWEndpoint.JsonParser,
        (req, res) => this.put(req, res));
    }
  }
}

CMWEndpoint.JsonParser = express.json({ strict: false });

module.exports = CMWEndpoint;
