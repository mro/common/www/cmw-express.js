
import EventEmitter from 'events'
import { IRouter } from 'express'

export = CMWServer;
export as namespace cmwexpress;

declare class CMWServer {
  constructor(config: cmwexpress.Config)
    
  register(router: IRouter): IRouter
  registerDevice(router: IRouter, config: cmwexpress.EPConfig): void
  release(): void

  config: cmwexpress.Config
  endpoints: { [path: string]: cmwexpress.CMWEndpoint }
}

declare namespace CMWServer {
  interface EPConfig {
    device: string,
    property: string,
    server?: string,
    path?: string,
    unsubscribeDelay?: number,
    throttle?: number,
    selector?: string
    read?: boolean,
    write?: boolean,
    monitor?: boolean
  }

  interface Config {
    endpoints: EPConfig[]
  }
  
  class CMWEndpoint extends EventEmitter {
    constructor(config: EPConfig)
    
    release(): void
    register(router: IRouter, path: string): void

    deviceName: string
    propName: string
    config: EPConfig

    value: any
  }
}