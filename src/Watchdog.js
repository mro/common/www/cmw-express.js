// @ts-check
const
  debug = require('debug')('cmw:watchdog'),
  EventEmitter = require('events');

class Watchdog extends EventEmitter {
  /**
   * @param {Number} [delay=10] watchdog delay in seconds
   * @param {Number} [maxDelay=3600] watchdog max delay in seconds
   */
  constructor(delay = 30, maxDelay = 3600) {
    super();
    this.delay = delay;
    this.maxDelay = maxDelay;
    this.errors = 0;
    /** @type {NodeJS.Timeout|null} */
    this.timer = null;
  }

  reset() {
    if (this.timer) {
      debug('clearing watchdog');
      clearTimeout(this.timer);
      this.timer = null;
    }
    if (this.errors !== 0) {
      debug('clearing error count(%i)', this.errors);
      this.errors = 0;
    }
  }

  _onTimeout() {
    this.timer = null;
    this.emit('expired');
  }

  start() {
    if (!this.timer) {
      const delay = Math.min(this.delay * Math.pow(2, this.errors++),
        this.maxDelay);
      debug('starting watchdog: %i error(s), waiting %i seconds', this.errors,
        delay);
      this.timer = setTimeout(() => this._onTimeout(), delay * 1000);
    }
  }
}

module.exports = Watchdog;
