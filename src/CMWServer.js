// @ts-check

const
  { get, forEach } = require('lodash');

const CMWEndpoint = require('./CMWEndpoint');

/**
 * @typedef {import('express').IRouter} IRouter
 */

class CMWServer {
  /**
   * @param {cmwexpress.Config} config
   */
  constructor(config) {
    this.config = config;
    /** @type {{ [path: string]: CMWEndpoint }} */
    this.endpoints = {};
  }

  /**
   * @param {IRouter} router
   */
  register(router) {
    this.release();
    forEach(this.config.endpoints,
      (config) => this.registerDevice(router, config));
    return router;
  }

  /**
   * @param {IRouter} router
   * @param {cmwexpress.EPConfig} config
   */
  registerDevice(router, config) {
    const path = get(config, [ 'path' ], `/${config.device}/${config.property}`);

    const ep = new CMWEndpoint(config);
    ep.register(router, path);

    this.endpoints[path] = ep;
  }

  release() {
    forEach(this.endpoints, (ep) => {
      ep.release();
    });
    this.endpoints = {};
  }

}
module.exports = CMWServer;
