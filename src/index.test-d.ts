import { expectType } from 'tsd';

import { default as express } from 'express'
import CMWServer from '.';


(async function() {
  const server = new CMWServer({
    endpoints: [
      { device: 'test', property: 'test', throttle: 250 }
    ]
  });
  var app = express();
  server.register(app);

  server.release();
  expectType<cmwexpress.CMWEndpoint>(server.endpoints['test']);
})();