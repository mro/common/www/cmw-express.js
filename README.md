# CMW Express

[CMW](https://wikis.cern.ch/display/MW/CMW+-+Controls+Middleware) plugin for [Express.js](https://expressjs.com/).

# Usage

To use this library as a dependency:
```bash
# Install it
npm install "git+https://gitlab.cern.ch/mro/common/www/cmw-express.js.git"
```

To load endpoints on your Express.js application:
```js
const
  express = require('express'),
  CMWServer = require('@cern/cmw-express'),
  config = require('./config.js');

const app = express();
const service = new CMWServer(config);
service.register(app);

app.listen(8080);

// Or using a router
const router = express.Router();
service.register(router);
app.use('/api', router);
```

Mounted endpoints will serve json encoded values using the following methods:
- **GET** returning latest value, (direct, no subscription)
- **SSE** returning current value, then throttled updates
- **WebSocket** returning current value, then throttled updates

**Note:** BigInt values from device are encoded as string in the JSon payload

# Configuration

The configuration contains the following information:
```ts
interface Config {
  endpoints: Array<{
    // RDA3 device name
    device: string,
    // RDA3 property name
    property: string,
    // RDA3 user selector
    selector?: string
    // RDA3 server (optional)
    server?: string,
    // endpoint serving path (default: `/device/property`)
    path?: string,
    // idle delay before unsubscribing in seconds (default: always subscribed)
    unsubscribeDelay?: number,
    // update events throttle in milliseconds (default: not throttling)
    throttle?: number,
    // support read (GET) primitives (default: true)
    read: boolean,
    // support write (PUT) primitives (default: false)
    write: boolean,
    // support subscribe (WS/SSE) primitives (default: true)
    monitor: boolean
  }>
}
```

# Examples:

Example configuration (_./config.js_):
```js
{
  endpoints: [
    { device: 'TCSG.252.TEST', property: 'ExpertAcquisition' }
  ]
}
```

Associated application:
```js
const
  express = require('express'),
  ews = require('express-ws'), // enable websocket support
  CMWServer = require('@cern/cmw-express'),
  config = require('./config.js');

const app = express();
ews(app);

const service = new CMWServer(config);
service.register(app);
app.listen(8080);
```

Example request:
```bash
$ curl -s http://localhost:8080/TCSG.252.TEST/ExpertAcquisition | json_pp
{
   "updateType" : "UT_NORMAL",
   "acqStamp" : "1616744782339819000",
   "cycleStamp" : "0",
   "data" : {
      "entries" : {
         "maximumMDCSensorsReadingProcessTime" : 0,
         "lvdt2_left_downstream" : 0,
         "interferometer_gap_upstream" : 0,
         "lvdt_left_downstream" : 15.0405612785499,
         "timestampAtTriggerPRS" : "25",
         "maximumPRSSurveyProcessTime" : 7,
         "betaActiveIP" : 0,
         "actualMDCCommandProcessTime" : 0,
         "maximumPRSLimitsReadingProcessTime" : 7,
         "currentBeamEnergy" : "450000",
         "bets_gap_upstream" : 0,
...
```

## Advanced Example

Application:
```js
const
  express = require('express'),
  ews = require('express-ws'),
  CMWServer = require('@cern/cmw-express');

const app = express();

ews(app);

const cmw = new CMWServer({
  endpoints: [
    { device: 'TCSG.252.TEST', property: 'ExpertAcquisition', path: '/acquisition' },
    { device: 'TCSG.252.TEST', property: 'ExpertSetting', path: '/setting', write: true }
  ]
});

cmw.register(app);


app.listen(8080, () => console.log('listening on http://localhost:8080'));
```

Example requests:
```bash
$ curl -s -X PUT \
  -H 'Content-type: application/json' \
  --data-binary '{ "acquisitionDelayMDC": 500 }' \
  'http://localhost:8080/setting' | jq
"done"

$ curl -s -X GET \
  'http://localhost:8080/setting' | jq .data.entries.acquisitionDelayMDC
500
```

# Tests

To run the unit-tests:
```bash
npm run test
```
